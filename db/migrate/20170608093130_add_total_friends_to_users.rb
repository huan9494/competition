class AddTotalFriendsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :total_friends, :integer, default: 0
  end
end
