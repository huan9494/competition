class Contest < ApplicationRecord
  mount_uploader :picture, FileUploader

  has_many :user_contests
  has_many :users, through: :user_contests
end
