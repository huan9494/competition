class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  require 'fb_graph2'

  def facebook
    # You need to implement the method below in your model (e.g. app/models/user.rb)
    @user = User.from_omniauth(request.env["omniauth.auth"])
    access_token = request.env["omniauth.auth"]["credentials"]["token"]
    # # Returns true if the record is persisted, 
    # i.e. it’s not a new record and it was not destroyed, otherwise returns false.
    if @user.persisted?
      user_uid = @user.uid
      user = FbGraph2::User.new(user_uid).authenticate(access_token).fetch
      friend_count = user.friends.summary["total_count"]
      avatar_url = user.picture.url
      @user.update(total_friends: friend_count, avatar: avatar_url)
      
      sign_in_and_redirect @user#, :event => :authentication #this will throw if @user is not activated
    else
      session["devise.facebook_data"] = request.env["omniauth.auth"]
      redirect_to root_path
    end
  end

  def failure
    redirect_to root_path
  end

end