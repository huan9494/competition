class ContestsController < ApplicationController

  require 'fb_graph2'

  def index
    @enrolled_contests = current_user ? current_user.contests : []
    enrolled_contest_ids = @enrolled_contests.size > 0 ? @enrolled_contests.select(:id) : []
    @contests = Contest.where.not(id: enrolled_contest_ids)

  end

  def show
    @contest = Contest.find(params[:id])
  end

  def enroll
    @contest = Contest.find(params[:contest_id])

    current_user.contests << @contest unless current_user.contests.include?(@contest)
  end

  def dashboard
    enrolled_contest_ids = UserContest.where(user_id: current_user.id,
                                             status: "approved").select(:contest_id)
    @contests = Contest.where(id: enrolled_contest_ids )
  end

end