class Admin::ContestsController < ApplicationController

  before_action :authenticate_user!
  before_action :check_admin

  def index
    @contests = Contest.all
    @pending_user_contests = UserContest.all.length > 0 ? UserContest.where(status: "pending") : []
  end

  def show
    @contest = Contest.find(params[:id])
    approved_user_ids = UserContest.where(status: "approved").select(:user_id)
    @users = @contest.users.where(id: approved_user_ids )
  end

  def new
    @contest = Contest.new
  end

  def create
    @contest = Contest.new(contest_params)
    if @contest.save
      redirect_to admin_root_path
    else
      render 'new'
    end
  end

  def edit
    @contest = Contest.find(params[:id])
  end

  def update
    @contest = Contest.find(params[:id])
    if @contest.update(contest_params)
      redirect_to admin_root_path
    else
      render 'edit'
    end
  end

  def destroy
    @contest = Contest.find(params[:id])
    @contest.destroy
    redirect_to admin_root_path
  end

  def approve
    UserContest.where(contest_id: params[:contest_id], user_id: params[:user_id]).first.update(
                      status: "approved")
  end

  private
  def check_admin
    flash[:notice_admin] = "Your account is not allowed to access the admin page."
    redirect_to root_path unless current_user.admin?
  end

  def contest_params
    params.require(:contest).permit(:name, :description, :picture, :start_date, :end_date)
  end
end