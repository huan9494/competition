$(document).on('turbolinks:load', function(){
  $(".btn_join").on('click', function(){
    contest_id = this.getAttribute("contest_id");
    user_data = this.getAttribute("user_data");
    if (user_data.length > 0) {
      $("#card_" + contest_id).fadeOut(300);
    $.ajax({
        url: '/contests/enroll',
        dataType: 'script',
        method: 'post',
        data: {contest_id}
      });
    } else {
      alert("You must sign in before joining the contest");
      window.location.replace("/users/sign_in")
    }
  });

  $(".btn_approve").on('click', function(){
    contest_id = this.getAttribute("contest_id");
    user_id = this.getAttribute("user_id");
    $("#admin_row_" + contest_id).fadeOut(300);
    $.ajax({
      url: '/admin/contests/approve',
      dataType: 'script',
      method: 'patch',
      data: {contest_id, user_id}
    });
  });

  $(".btn-pref .btn").click(function () {
    $(".btn-pref .btn").removeClass("btn-primary").addClass("btn-default");
    $(this).removeClass("btn-default").addClass("btn-primary");   
});

})