Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }

  namespace :admin do
    resources :contests do
        collection do
          patch :approve
        end
    end
    root "contests#index"
  end

  resources :contests, only:[:index, :show] do
    collection do
      post :enroll
      get :dashboard
    end
  end
  root "contests#index"
end
